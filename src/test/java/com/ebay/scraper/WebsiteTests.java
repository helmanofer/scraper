package com.ebay.scraper;

import org.junit.jupiter.api.Test;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


class WebsiteTests {
	@Test
	void testWebsite() {
		String testHtml = "<html>\n" +
				"    <head>\n" +
				"        <title>TITLE</title>\n" +
				"    </head>\n" +
				"    <body>\n" +
				"        <ul>\n" +
				"            <li>\n" +
				"                <a href=http://test2.com></a>\n" +
				"            </li>\n" +
				"        </ul>\n" +
				"    </body>\n" +
				"</html>";
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		JmsTemplate jmsTemplateMock = mock(JmsTemplate.class);
		Website testWebsite = new Website("http://test.com", 10);
		when(restTemplateMock.getForObject(anyString(), eq(String.class))).thenReturn(testHtml);
		doNothing().when(jmsTemplateMock).convertAndSend(anyString(), any(Website.class));
		testWebsite.crawl(restTemplateMock, jmsTemplateMock);

		List<String> actual = Collections.singletonList("http://test2.com");
		assertThat(testWebsite.getLinks(), is(actual));
	}

}
