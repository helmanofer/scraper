package com.ebay.scraper;

import java.io.IOException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.Registry;

@Configuration
@ConfigurationProperties(prefix = "http-client")
public class RestTemplateConfig {
    private int maxConcurrentPerRoute;
    private int maxConcurrentTotal;
    private int numRetry;
    private int retryTimeoutSec;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(clientHttpRequestFactory());
    }

    public int getRetryTimeoutSec() {
        return retryTimeoutSec;
    }

    public void setRetryTimeoutSec(int retryTimeoutSec) {
        this.retryTimeoutSec = retryTimeoutSec;
    }

    public int getNumRetry() {
        return numRetry;
    }

    public void setNumRetry(int numRetry) {
        this.numRetry = numRetry;
    }

    public int getMaxConcurrentTotal() {
        return maxConcurrentTotal;
    }

    public void setMaxConcurrentTotal(int maxConcurrentTotal) {
        this.maxConcurrentTotal = maxConcurrentTotal;
    }

    public int getMaxConcurrentPerRoute() {
        return maxConcurrentPerRoute;
    }

    public void setMaxConcurrentPerRoute(int maxConcurrentPerRoute) {
        this.maxConcurrentPerRoute = maxConcurrentPerRoute;
    }

    @Bean
    public ServiceUnavailableRetryStrategy getServiceUnavailableRetryHnadler() {
        return new ServiceUnavailableRetryStrategy() {

            @Override
            public boolean retryRequest(org.apache.http.HttpResponse response, int executionCount,
                    HttpContext context) {
                return executionCount < numRetry;
            }

            @Override
            public long getRetryInterval() {
                return retryTimeoutSec;
            }
        };
    }

    @Bean
    public HttpRequestRetryHandler getRetryHandler() {
        return new HttpRequestRetryHandler() {
            @Override
            public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
                return executionCount < numRetry;
            }
        };
    }

    @Bean
    public PoolingHttpClientConnectionManager getConnectionManager() {
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
            .register("http", PlainConnectionSocketFactory.INSTANCE)
            .register("https", new SSLConnectionSocketFactory(SSLContexts.createSystemDefault()))
            .build();

        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        connManager.setDefaultMaxPerRoute(maxConcurrentPerRoute);
        connManager.setMaxTotal(maxConcurrentTotal);
        return connManager;
    }
 
    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        int timeout = 5000;
        RequestConfig config = RequestConfig.custom()
        .setConnectTimeout(timeout)
        .setConnectionRequestTimeout(timeout)
        .setSocketTimeout(timeout)
        .build();
        
        
        CloseableHttpClient client = HttpClientBuilder
        .create()
        .setConnectionManager(getConnectionManager())
        .setDefaultRequestConfig(config)
        .setRetryHandler(getRetryHandler())
        .setServiceUnavailableRetryStrategy(getServiceUnavailableRetryHnadler())
        .build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }
}