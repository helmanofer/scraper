package com.ebay.scraper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.JmsListenerEndpointRegistry;
import org.springframework.context.support.AbstractApplicationContext;

@SpringBootApplication
@EnableJms
@EnableConfigurationProperties({RestTemplateConfig.class})
public class Application implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    JmsTemplate jmsTemplate;
    JmsListenerEndpointRegistry jmsListenerEndpointRegistry;

    private final ApplicationContext appContext;

    public Application(ApplicationContext appContext) {
        this.appContext = appContext;
    }

    @Value("${input.path}")
    private String inputPath;

    @Autowired
    public void setJmsTemplate(final JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Autowired
    public void setJmsListenerEndpointRegistry(final JmsListenerEndpointRegistry jmsListenerEndpointRegistry) {
        this.jmsListenerEndpointRegistry = jmsListenerEndpointRegistry;
    }

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public int getTotalPendingMessages() {
        return jmsTemplate.browse("queue", (session, browser) ->
                Collections.list(browser.getEnumeration()).size());
    }

    private List<Website> readInputFile() throws IOException {
        List<Website> csvData = new ArrayList<>();
        BufferedReader csvReader = new BufferedReader(new FileReader(inputPath));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            csvData.add(new Website(data[0], Integer.parseInt(data[1].replace(" ", ""))));
        }
        csvReader.close();
        return csvData;
    }

    private void waitForDone() {
        // Samples the queue and checks if there are un-commit messages
        int totalPendingMessages = getTotalPendingMessages();
        while (totalPendingMessages > 0) {
            logger.info(String.format("TotalPendingMessages: %s", totalPendingMessages));
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException ignore) {
            }
            totalPendingMessages = getTotalPendingMessages();
        }
    }

    private void destroy() {
        for (final MessageListenerContainer listenerContainer : jmsListenerEndpointRegistry.getListenerContainers()) {
            final DefaultMessageListenerContainer container = (DefaultMessageListenerContainer) listenerContainer;
            container.destroy();
        }
        ((AbstractApplicationContext) appContext).close();
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<Website> csvData = readInputFile();
        csvData.forEach((website) -> {
            logger.info(String.format("sending: %s", website.getUrl()));
            jmsTemplate.convertAndSend("queue", website);
        });

        waitForDone();
        destroy();
    }
}