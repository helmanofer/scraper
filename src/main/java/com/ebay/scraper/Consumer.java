package com.ebay.scraper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.file.Path;

@Component
public class Consumer {
    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @Value("${output.path}")
    private Path outputPath;

    final RestTemplate restTemplate;
    final JmsTemplate jmsTemplate;


    public Consumer(RestTemplate restTemplate, JmsTemplate jmsTemplate) {
        this.restTemplate = restTemplate;
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = "queue", containerFactory = "ebayFactory")
    public void receiveMessage(Website website) {
        logger.info("Received <" + website + ">");
        website.setOutputPath(getOutputPath());
        website.crawl(restTemplate, jmsTemplate);
    }

    public Path getOutputPath() {
        return outputPath;
    }

    public void setOutputPath(Path outputPath) {
        this.outputPath = outputPath;
    }
}