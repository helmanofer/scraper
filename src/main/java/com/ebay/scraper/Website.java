package com.ebay.scraper;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.client.RestTemplate;

public class Website {
    private static final Logger logger = LoggerFactory.getLogger(Website.class);

    private String url;
    private int numPages;
    private String body;
    private Path outputPath = null;

    public List<String> getLinks() {
        return links;
    }

    private List<String> links = new ArrayList<>();

    public Website() {

    }
    
    public Website(String url, int numPages) {
        this.setUrl(url);
        this.setNumPages(numPages);
    }

    public Website(String url) {
        this.setUrl(url);
        this.setNumPages(0);
    }
    
    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }

    @Override
    public String toString() {
        return String.format("Website{url=%s}", url);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return this.url.toLowerCase().replaceAll("[^A-Za-z0-9]","");
    }

    public void crawl(RestTemplate restTemplate, JmsTemplate jmsTemplate) {
        try {
            downloadWebsite(restTemplate);
            saveToFile();
            parseLinks();
            produceNewWebsites(jmsTemplate);
        } catch (IOException e) {
            logger.error(String.format("Failed to crawl %s", getUrl()), e);
        }
    }
    
    private void produceNewWebsites(JmsTemplate jmsTemplate) {
        links.forEach((link) -> {
            Website website = new Website(link, 0);
            logger.info(String.format("sending %s", website.getUrl()));
            jmsTemplate.convertAndSend("queue", website);
        });
    }

    private void downloadWebsite(RestTemplate restTemplate) {
        long start = System.currentTimeMillis();
        body = restTemplate.getForObject(getUrl(), String.class);
        long end = System.currentTimeMillis();
        logger.info(String.format("time taken to download site %s: %s ms", getUrl(), (end - start)));
    }

    private void saveToFile() throws IOException {
        String fileName = getFileName();
        Path filePath;
        if (outputPath != null) {
            filePath = Paths.get(outputPath.toString(), fileName);
        } else {
            filePath = Paths.get(fileName);
        }
        logger.info(String.format("Saving %s to %s", getUrl(), filePath));
        BufferedWriter writer = new BufferedWriter(new FileWriter(filePath.toString()));
        writer.write(body);
        writer.close();
    }

    private void parseLinks() {
        if (getNumPages() > 0) {
            Document doc = Jsoup.parse(body, getUrl());
            Elements elinks = doc.select("a[href]");

            elinks.forEach((elink) -> links.add(elink.attr("href")));
            links = links.stream()
                        .filter(thisUrl -> thisUrl.contains("http"))
                        .limit(getNumPages())
                        .collect(Collectors.toList());
            logger.info(String.format("Parsed links %s", links.toString()));
        }
    }

	public void setOutputPath(Path outputPath) {
        this.outputPath = outputPath;
	}
} 