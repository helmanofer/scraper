package com.ebay.scraper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;

public class ConsumerErrorHandler implements ErrorHandler{
    private static final Logger log = LoggerFactory.getLogger(ConsumerErrorHandler.class);

    @Override
    public void handleError(Throwable t) {
        log.error("Error occured during handling message", t);
    }
}
